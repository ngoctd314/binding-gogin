package main

import (
	"bindinggogin/requestcontext"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		v, err := requestcontext.NewVideo(c)
		c.JSON(200, gin.H{
			"video": v,
			"error": err,
		})

	})

	r.Run(":8080")
}
