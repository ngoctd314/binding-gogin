package requestcontext

import "github.com/gin-gonic/gin"

type director struct {
	builder
}

func newDirector(b builder) *director {
	return &director{
		builder: b,
	}
}

// exec all method in builder interface
func (d *director) build(c *gin.Context) error {
	var err error
	if err = d.builder.setHeader(c); err != nil {
		return err
	}
	if err = d.builder.setQuery(c); err != nil {
		return err
	}

	return nil
}
