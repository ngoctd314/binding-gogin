package requestcontext

import (
	"github.com/gin-gonic/gin"
)

type builder interface {
	setHeader(*gin.Context) error
	setQuery(*gin.Context) error
}
