package requestcontext

import "github.com/gin-gonic/gin"

type videoHeader struct {
	IP     string `header:"X-Client-Rip"`
	Origin string `header:"Origin"`
	UA     string `header:"User-Agent"`
}

var _ rcValidator = &videoHeader{}

func newVideoHeader(c *gin.Context) (*videoHeader, error) {
	// binding
	videoHeader := &videoHeader{}
	err := c.ShouldBindHeader(videoHeader)
	if err != nil {
		return nil, err
	}

	// validate
	err = videoHeader.execValidate()
	if err != nil {
		return nil, err
	}

	return videoHeader, nil
}

func (v *videoHeader) execValidate() error {
	return execValidate(v, validateUA)
}

func validateUA(i interface{}) error {
	v := i.(*videoHeader)

	if v.UA == "" {
		v.UA = "chrome"
	}

	return nil
}
