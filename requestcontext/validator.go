package requestcontext

type rcValidator interface {
	execValidate() error
}

type validateWiths func(interface{}) error

// execValidate execute all validate options
func execValidate(v interface{}, validateWiths ...validateWiths) error {
	var err error
	for _, validateWith := range validateWiths {
		err = validateWith(v)
		if err != nil {
			return err
		}
	}
	return nil
}
